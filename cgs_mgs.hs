newtype Column a = Column [a] deriving (Eq, Show)
instance Num a => Num (Column a) where
  Column x + Column y    = Column $ zipWith (+) x y
  Column x - Column y    = Column $ zipWith (-) x y
  Column x * Column y    = Column $ zipWith (*) x y
  abs (Column x)         = Column $ map abs x
  signum (Column x)      = Column $ map signum x
  fromInteger x          = Column [fromInteger x]

newtype Matrix a = Matrix [Column a] deriving (Eq)
instance Num a => Num (Matrix a) where
  Matrix x + Matrix y = Matrix $ zipWith (+) x y
  Matrix x - Matrix y = Matrix $ zipWith (-) x y
  Matrix x * Matrix y = Matrix $ zipWith (*) x y
  abs (Matrix x)      = Matrix $ map abs x
  signum (Matrix x)   = Matrix $ map signum x
  fromInteger x       = Matrix [fromInteger x]
instance Show a => Show (Matrix a) where
  show (Matrix a) = "Matrix {\n" ++ (concatMap (++ ",\n") $ map show a) ++ "}"

getValue_C :: Int -> Column d -> d
getValue_C i (Column d) = d!!i

getColumn :: Int -> Matrix d -> Column d
getColumn i (Matrix a) = a!!i

getValue_M :: Int -> Int -> Matrix d -> d
getValue_M col row a = getValue_C row $ getColumn col a

dim :: Matrix d -> (Int, Int)
dim (Matrix a) = (rows, columns)
  where
    Column x = getColumn 0 (Matrix a)
    rows = length x
    columns = length a

nOfRow :: Matrix d -> Int
nOfRow = fst . dim

nOfCol :: Matrix d -> Int
nOfCol = snd . dim

innerProduct :: Num d => Column d -> Column d -> d
innerProduct a b = sum products
  where
    Column products = a * b

(*^) :: Num d => d -> Column d -> Column d
c *^ (Column a) = Column $ map (* c) a

normalise :: (Num d, Floating d) => Column d -> Column d
normalise (Column a) = Column $ map (/ length) a
  where
    Column squares = (Column a) ^ 2
    length         = sqrt $ sum squares

transpose :: Num d => Matrix d -> Matrix d
transpose a = Matrix [Column [getValue_M i j a | i <- [0..nOfCol a - 1]] | j <- [0.. nOfRow a - 1]]

matMul :: Num d => Matrix d -> Matrix d -> Matrix d
matMul a b = Matrix [Column [innerProduct (getColumn i a_t) (getColumn j b) | i <- [0..nOfRow a - 1]] | j <- [0..nOfCol b - 1]]
  where
    a_t = transpose a

identityMatDouble :: Int -> Matrix Double
identityMatDouble n = Matrix [Column[if i == j then 1 else 0| i <- [0..n-1]] | j <- [0..n-1]]

cgs_get_v :: (Num d, Floating d) => Int -> Matrix d -> Column d
cgs_get_v 0 a = getColumn 0 a
cgs_get_v i a = col_i - sum' [(innerProduct (col_i) (cgs_get_q j a)) *^ (cgs_get_q j a) | j <- [0..i-1]]
  where
    col_i = getColumn i a
    sum' = foldr (+) $ Column [fromInteger 0 | _ <- [0..nOfRow a - 1]]

cgs_get_q :: (Num d, Floating d) => Int -> Matrix d -> Column d
cgs_get_q i a = normalise $ cgs_get_v i a

cgs_get_Q :: (Num d, Floating d) => Matrix d -> Matrix d
cgs_get_Q a = Matrix [cgs_get_q i a| i <- [0..(nOfCol a) - 1]]

mgs_get_v :: (Num d, Floating d) => Int -> Int -> Matrix d -> Column d
mgs_get_v i 0 a = getColumn i a
mgs_get_v j i a = v - (innerProduct v q *^ q)
                    where
                      v = mgs_get_v j (i - 1) a
                      q = mgs_get_q (i - 1) a

mgs_get_q :: (Num d, Floating d) => Int -> Matrix d -> Column d
mgs_get_q 0 a = normalise $ mgs_get_v 0 0 a
mgs_get_q i a = normalise $ mgs_get_v i i a

mgs_get_Q :: (Num d, Floating d) => Matrix d -> Matrix d
mgs_get_Q a = Matrix [mgs_get_q i a| i <- [0..(nOfCol a) - 1]]

test :: Matrix Double
test = Matrix [Column [1.0, 2.0, 3.0],
               Column [4.0, 5.0, 6.0]]

test2 :: Matrix Double
test2 = Matrix [Column [7.0, 8.0],
                Column [9.0, 10.0],
                Column [11.0, 12.0]]

k_1 :: Matrix Double
k_1 = Matrix [Column [0, 0, 0, 0.1, 1],
              Column [0, 0, 0.1, 0, 1],
              Column [0, 0.1, 0, 0, 1],
              Column [0.1, 0, 0, 0, 1]]

k_5 :: Matrix Double
k_5 = Matrix [Column [0, 0, 0, 0.00001, 1],
              Column [0, 0, 0.00001, 0, 1],
              Column [0, 0.00001, 0, 0, 1],
              Column [0.00001, 0, 0, 0, 1]]

k_10 :: Matrix Double
k_10 = Matrix [Column [0, 0, 0, 0.0000000001, 1],
              Column [0, 0, 0.0000000001, 0, 1],
              Column [0, 0.0000000001, 0, 0, 1],
              Column [0.0000000001, 0, 0, 0, 1]]

printCalc :: Matrix Double -> IO ()
printCalc a = do putStrLn "For CGS:"
                 putStrLn "Q:"
                 putStrLn $ show $ cgs_get_Q a
                 putStrLn ""
                 putStrLn "Q_transpose * Q - I^(n):"
                 putStrLn $ show $ matMul (transpose (cgs_get_Q a)) (cgs_get_Q a) - identityMatDouble (nOfCol a)
                 putStrLn ""
                 putStrLn ""
                 putStrLn "For MGS:"
                 putStrLn "Q:"
                 putStrLn $ show $ mgs_get_Q a
                 putStrLn ""
                 putStrLn "Q_transpose * Q - I^(n):"
                 putStrLn $ show $ matMul (transpose (mgs_get_Q a)) (mgs_get_Q a) - identityMatDouble (nOfCol a)


main :: IO ()
main = do putStrLn "For k = 10^-1"
          putStrLn "==========================="
          printCalc k_1
          putStrLn "==========================="
          putStrLn ""
          putStrLn "For k = 10^-5"
          putStrLn "==========================="
          printCalc k_5
          putStrLn "==========================="
          putStrLn ""
          putStrLn "For k = 10^-10"
          putStrLn "==========================="
          printCalc k_10
          putStrLn "==========================="
          putStrLn ""
