module cgs_mgs

import Data.Vect

implementation Num a => Num (Vect n a) where
  (+) = zipWith (+)
  (*) = zipWith (*)
  fromInteger n = replicate _ (fromInteger n)

implementation Neg a => Neg (Vect n a) where
  (-) = zipWith (-)
  negate = map negate

implementation Fractional a => Fractional (Vect n a) where
  (/) = zipWith (/)

Mat : (rows : Nat) -> (cols : Nat) -> (elem : Type) -> Type
Mat n m e = Vect n (Vect m e)

get : Nat -> Vect m a -> a
get Z (x :: xs) = x
get (S k) (x :: xs) = get k xs

get_row : Nat -> Mat n m a -> Vect m a
get_row Z (x :: xs) = x
get_row (S k) (x :: xs) = get_row k xs

get_col : Nat -> Mat n m a -> Vect n a
get_col k = map (get k)

transpose_mat : Mat n m a -> Mat m n a
transpose_mat [] = replicate _ []
transpose_mat (x :: xs) = zipWith (::) x (transpose_mat xs)

inner_prod : Num a => Vect n a -> Vect n a -> a
inner_prod [] [] = 0
inner_prod (x :: xs) (y :: ys) = (x * y) + inner_prod xs ys

vect_mul_mat : Num a => Vect m a -> Mat p m a -> Vect p a
vect_mul_mat _ [] = []
vect_mul_mat x (v :: vs) = inner_prod x v :: vect_mul_mat x vs

mat_mul : Num a => Mat n m a -> Mat m p a -> Mat n p a
mat_mul [] [] = []
mat_mul [] (x :: xs) = []
mat_mul (x :: xs) y = vect_mul_mat x (transpose_mat y) :: mat_mul xs y

normalise : (Num a, Fractional a) => Vect n a -> Vect n a
normalise {n} v = map (/ len) v
                    where
                      len = sum [(get (x-1) v) * (get (x-1) v) | x <- [1..(cast n]]

-- CGS
q_cgs : (Num a, Neg a) => Nat -> Mat n m a -> Vect n a
q_cgs i x = normalise (v_cgs i x)

v_cgs : (Num a, Neg a) => Nat -> Mat n m a -> Vect n a
v_cgs Z x = get_col Z x
v_cgs {m} i x = v_cgs' (transpose_mat x)
                      where
                        v_cgs' j = get_row i j - sum' i
                          where
                            sum' Z = map (* inner_prod (get_col i x) (get_row Z j)) (get_row Z j)
                            sum' (S k) = map (* inner_prod (get_col i x) (get_row k j)) (get_row k j) + sum' k  

test1 : Mat 2 3 Int
test1 = [[1,2,3],
        [4,5,6]]

test2 : Mat 3 2 Int
test2 = [[7,8],
         [9,10],
         [11,12]]
